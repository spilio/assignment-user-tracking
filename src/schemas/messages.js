/**
 * Message model
 */

const Joi = require('joi');

/**
 * 
 */
const Message = Joi.object().keys({
  id: Joi.string()
    .description('The message Id'),
  type: Joi.string().valid('event', 'query', 'command').required(),
  ts: Joi.date().iso()
}).meta({
  className: 'Message',
  description: 'Base message model'
});

/**
 * Event
 */
const Event = Message.keys({
  name: Joi.string()
    .description('The event name')
}).meta({
  className: 'Event',
  description: 'Event schema'
});


/**
 * Command
 */
const Command = Message.keys({
  cmd: Joi.string()
    .description('The command name'),
  data: Joi.any()
}).meta({
  className: 'Command',
  description: 'Command schema'
});

/**
 * Query
 */
const Query = Message.keys({
  name: Joi.string()
    .description('The name of the query'),
    data: Joi.any()
}).meta({
  className: 'Query',
  description: 'Query schema'
});

module.exports.Message = Message;
module.exports.Event = Event;
module.exports.Command = Command;
module.exports.Query = Query;