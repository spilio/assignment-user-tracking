const MessageHandler = require('../../message_handler');

/**
 * Command handler for assigning 
 * properties to users.
 */
class AssignPropertiesToUserCmd extends MessageHandler {

    constructor({
        loggingService,
        gamesService
    } = {}) {
        super();

        this.log = loggingService.instance;
        this.gamesService = gamesService;
    }

    type() {
        return 'command';
    }

    supports(message) {
        // check command name
        return (message.cmd === 'assign_properties_to_user');
    }

    /**
     * Handles the command
     * @param {*} message 
     */
    async handle(message) {

        this.log.debug('Received command to assign key-value properties to a user for a given game');

        const { data } = message;

        const {
            username,
            game_code,
            properties
        } = data;

        // assign properties to user for game
        await this.gamesService.assignPropertiesToUser(
            username,
            game_code,
            properties
        );

    }


}

module.exports = AssignPropertiesToUserCmd;
