const MessageHandler = require('../../message_handler');

/**
 * Command handler for creating new games.
 */
class CreateGameCmd extends MessageHandler {

    constructor({
        loggingService,
        gamesService
    } = {}) {
        super();

        this.log = loggingService.instance;
        this.gamesService = gamesService;
    }

    type() {
        return 'command';
    }

    supports(message) {
        // check command name
        return (message.cmd === 'create_game');
    }

    /**
     * Handles the command
     * @param {*} message 
     */
    async handle(message) {

        this.log.debug('Received command to create a new game');

        const { data } = message;
        const {
            code,
            title
        } = data;

        // register the new game
        await this.gamesService.registerGame({
            code,
            title
        });

    }


}

module.exports = CreateGameCmd;
