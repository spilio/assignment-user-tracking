const MessageHandler = require('../../message_handler');

/**
 * Command handler for stopping a game session.
 */
class StopSessionCmd extends MessageHandler {

    constructor({
        loggingService,
        gamesService
    } = {}) {
        super();

        this.log = loggingService.instance;
        this.gamesService = gamesService;
    }

    type() {
        return 'command';
    }

    supports(message) {
        // check command name
        return (message.cmd === 'stop_session');
    }

    /**
     * Handles the command
     * @param {*} message 
     */
    async handle(message) {

        this.log.debug('Received command to stop a game session');

        const { data } = message;
        const {
            username,
            game_code
        } = data;

        // stop the session
        await this.gamesService.stopSession(
            username,
            game_code
        );

    }


}

module.exports = StopSessionCmd;
