const MessageHandler = require('../../message_handler');

/**
 * Command handler for starting a new game session.
 */
class StartSessionCmd extends MessageHandler {

    constructor({
        loggingService,
        gamesService
    } = {}) {
        super();

        this.log = loggingService.instance;
        this.gamesService = gamesService;
    }

    type() {
        return 'command';
    }

    supports(message) {
        // check command name
        return (message.cmd === 'start_session');
    }

    /**
     * Handles the command
     * @param {*} message 
     */
    async handle(message) {

        this.log.debug('Received command to start a new game session');

        const { data } = message;
        const {
            username,
            game_code
        } = data;

        // start a new session
        await this.gamesService.startSession(
            username,
            game_code
        );

    }


}

module.exports = StartSessionCmd;
