const MessageHandler = require('../../message_handler');

/**
 * Command handler for assigning 
 * users to games.
 */
class AssignUserToGameCmd extends MessageHandler {

    constructor({
        loggingService,
        gamesService
    } = {}) {
        super();

        this.log = loggingService.instance;
        this.gamesService = gamesService;
    }

    type() {
        return 'command';
    }

    supports(message) {
        // check command name
        return (message.cmd === 'assign_user_to_game');
    }

    /**
     * Handles the command
     * @param {*} message 
     */
    async handle(message) {

        this.log.debug('Received command to assign a user to a game');

        const { data } = message;
        const {
            username,
            game_code
        } = data;

        // register the new user
        await this.gamesService.assignUserToGame({
            username,
            game_code
        });

    }


}

module.exports = AssignUserToGameCmd;
