const MessageHandler = require('../../message_handler');

/**
 * Command handler for creating new users.
 */
class CreateUserCmd extends MessageHandler {

    constructor({
        loggingService,
        usersService
    } = {}) {
        super();

        this.log = loggingService.instance;
        this.usersService = usersService;
    }

    type() {
        return 'command';
    }

    supports(message) {
        // check command name
        return (message.cmd === 'create_user');
    }

    /**
     * Handles the command
     * @param {*} message 
     */
    async handle(message) {

        this.log.debug('Received command to create a new user');

        const { data } = message;
        const {
            username,
            first_name,
            last_name,
            avatar
        } = data;

        // register the new user
        await this.usersService.registerUser({
            username,
            first_name,
            last_name,
            avatar
        });

    }


}

module.exports = CreateUserCmd;
