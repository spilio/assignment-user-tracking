const MessageHandler = require('../../message_handler');

/**
 * Query handler for fetching user properties in a game.
 */
class GetUserPropertiesQuery extends MessageHandler {

    constructor({
        loggingService,
        gamesService
    } = {}) {
        super();

        this.log = loggingService.instance;
        this.gamesService = gamesService;
    }

    type() {
        return 'query';
    }

    supports(message) {
        // check query name
        return (message.name === 'get_user_properties');
    }

    /**
     * Handles the query
     * @param {*} message 
     */
    async handle(message) {

        this.log.debug('Received query to get user properties in game');

        const { data } = message;

        let {
            username,
            game_code,
            keys
        } = data;

        let results = await this.gamesService.getUserPropertiesInGame(username, game_code, { keys });

        return results;
    }


}

module.exports = GetUserPropertiesQuery;
