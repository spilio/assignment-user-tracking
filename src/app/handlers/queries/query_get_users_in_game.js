const MessageHandler = require('../../message_handler');

/**
 * Query handler for fetching users in a game.
 */
class GetUsersInGameQuery extends MessageHandler {

    constructor({
        loggingService,
        gamesService
    } = {}) {
        super();

        this.log = loggingService.instance;
        this.gamesService = gamesService;
    }

    type() {
        return 'query';
    }

    supports(message) {
        // check query name
        return (message.name === 'get_users_in_game');
    }

    /**
     * Handles the query
     * @param {*} message 
     */
    async handle(message) {

        this.log.debug('Received query to get all users in a game');

        const { data } = message;

        let {
            game_code,
            active
        } = data;

        // fetch all users in the given game
        let results = [];
        active = active || false;

        if (! active)
            results = await this.gamesService.getUsersInGame(game_code);
        else
            results = await this.gamesService.getActiveUsersInGame(game_code);

        return results;
    }


}

module.exports = GetUsersInGameQuery;
