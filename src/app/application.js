
/**
 * Application module.
 */
class Application {

  /**
   * Constructor
   * @param {[type]} optsLogging [description]
   * @param {[type]} server      [description]
   * @param {[type]} database    [description]
   */
  constructor({
    opts,
    loggingService,
    server
  } = {}) {

    this.loggingService = loggingService;
    this.server = server;
    this.opts = opts;
  }

  /**
   * Starts the application.
   * @return {Promise} [description]
   */
  async start(container) {
    try {

      // start the server
      await this.server.start(container);

    } catch (e) {
      console.error(`Error starting the application instance - ${e}`);
      process.exit(1);
    }
  }

}

module.exports = Application;
