
/**
 * Base message handler module.
 */
class MessageHandler {

    /**
    * Returns the message type
    */
    type() {
        throw new Error('Implement in subclass');
    }

    /**
     * Checks the message and returns
     * true if this handler can process it, 
     * false otherwise.
     * @param {*} message 
     */
    supports(message) {
        throw new Error('Implement in subclass');
    }

    /**
     * Handles the message.
     * @param {*} message 
     */
    handle(message) {
        throw new Error('Implement in subclass');
    }

}

module.exports = MessageHandler;
