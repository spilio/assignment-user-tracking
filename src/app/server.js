const _ = require('lodash');
const Hapi = require('@hapi/hapi');
const pluginHapiSwaggered = require('hapi-swaggered');
const Vision = require('@hapi/vision');
const Inert = require('@hapi/inert');
const Joi = require('joi');

/**
 * HTTP server module.
 *
 */
class Server {

  constructor({
    opts,
    loggingService,
    mongoService
  } = {}) {

    this.opts = opts;
    this.opts.PORT = process.env.PORT;

    this.logger = loggingService.instance;
    this.mongoService = mongoService;
  }

  /**
   * Returns the server instance
   * @return {[type]} [description]
   */
  get instance() {

    return this.server;
  }

  /**
   * Starts the server.
   * @return {Promise} [description]
   */
  async start(container) {

    if (_.isUndefined(this.opts.PORT)) {
      this.opts.PORT = 1330;
    }

    this.server = new Hapi.Server({
      port: this.opts.PORT
    });

    this.container = container;

    // setup the Swagger plugin
    await this.setupSwaggerPlugin();

    // connect to database
    await this.mongoService.connect();

    // set validator
    await this.server.validator(Joi);

    // start the server
    await this.server.start();
    
    /*
     * initialize controllers
     */
    _.each(container.registrations, (entry, key) => {
      if (key.includes('Controller')) {
        container.resolve(key);
      }
    });

    this.logger.info(`Server is up and listening at port ${this.opts.PORT}`);
  }

  /**
   * Sets up Hapi Swagger plugin
   * @return {Promise} [description]
   */
  async setupSwaggerPlugin() {

    /*
     * Swagger plugin options
     */
    const host = `localhost:${this.opts.PORT}`;

    await this.server.register([
      Inert,
      Vision,
      {
        plugin: pluginHapiSwaggered,
        options: {
          host,
          basePath: '/',
          schemes: ['http', 'https'],
          cors: false,
          info: {
            title: 'Wappier User Tracking API',
            description: 'Demo User Tracking API for the Software Architect assignment.',
            version: '1',
            contact: {
              name: 'Chrysanthos Spiliotopoulos',
              url: 'https://github.com/xrysanthos',
              email: 'chrysanthos.spiliotopoulos@gmail.com'
            },
            license: {
              name: 'MIT',
              url: 'https://opensource.org/licenses/MIT'
            }
          },
          consumes: ['application/json'],
          produces: ['application/json']
        }
      }
    ]);

  }


}

module.exports = Server;
