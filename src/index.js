/**
 * Main process
 */

// get the IoC container
const container = require('./container');

// get the application instance
const app = container.resolve('application');

// start the application instance
app.start(container);
