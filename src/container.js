const path = require('path');
const nconf = require('nconf');
const awilix = require('awilix');
const { Lifetime } = awilix;

/*
 * Load configuration environment
 */
nconf.argv().env('_').file(path.resolve(__dirname, '../config.json'));

// Create the container and set the injectionMode to PROXY (which is also the default).
const container = awilix.createContainer({
  injectionMode: awilix.InjectionMode.PROXY
});

/*
 * register variables
 */

container.register({
  opts: awilix.asValue(nconf.get())
});

// Load our modules!
container.loadModules([

  ['app/**/*.js', Lifetime.SINGLETON],
  ['services/**/*.js', Lifetime.SINGLETON],
  ['rest/**/*.js', Lifetime.SINGLETON]

], {

  cwd: __dirname,
  formatName: (name, descriptor) => {
    const {
      path
    } = descriptor;

    let suffix = '';

    if (path.includes('/rest/')) {
      suffix = 'Controller';
    } else if (path.includes('/services/')) {
      suffix = 'Service';
    } else if (path.includes('/handlers/')) {
      suffix = 'Handler';
    } 

    return `${name}${suffix}`;
  },
  resolverOptions: {
    lifetime: Lifetime.SINGLETON,
    register: awilix.asClass
  }
});


module.exports = container;
