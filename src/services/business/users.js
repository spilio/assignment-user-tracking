const _ = require('lodash');


class UsersService {

    /**
     * Constructor
     * @param {*} param0 
     */
    constructor({
        loggingService,
        mongoService
    } = {}) {
        this.log = loggingService.instance;
        this.dbService = mongoService;
    }

    /**
     * Creates a new user entity.
     * 
     * @param {*} param0 
     */
    async registerUser({
        username,
        first_name,
        last_name,
        avatar
    }) {

        const db = this.dbService.db();
        const col = db.collection('users');

        // check if username exists
        const filter = { username };
        let user = await col.findOne(filter);

        if (!_.isEmpty(user))
            throw new Error(`User with username [${username}] is already registered`);

        user = {
            username,
            first_name,
            last_name,
            avatar
        };

        // add timestamps
        const ts = new Date();
        user.created_at = ts;

        // add the entity to the store
        const ref = await col.insertOne(user);

        const id = ref.insertedId;

        this.log.debug(`New user registered with Id: ${id}`);

        return id;
    }

    /**
     * Searches and returns a user instance
     * by username.
     * @param {*} username 
     */
    async getUserByUsername(username) {

        const db = this.dbService.db();
        const col = db.collection('users');

        const filter = { username };
        const user = await col.findOne(filter);
        return user;
    }


 

}


module.exports = UsersService;