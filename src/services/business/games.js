const _ = require('lodash');


class GamesService {

    /**
     * Constructor
     * @param {*} param0 
     */
    constructor({
        loggingService,
        mongoService,
        usersService
    } = {}) {
        this.log = loggingService.instance;
        this.dbService = mongoService;
        this.usersService = usersService;
    }

    /**
     * Creates a new game entity.
     * 
     * @param {*} param0 
     */
    async registerGame({
        code,
        title
    }) {

        const db = this.dbService.db();
        const col = db.collection('games');

        // check if code name exists
        const filter = { code };
        let game = await col.findOne(filter);

        if (!_.isEmpty(game))
            throw new Error(`Game with code [${code}] is already registered`);

        game = {
            code,
            title
        };

        // add timestamps
        const ts = new Date();
        game.created_at = ts;

        // add the entity to the store
        const ref = await col.insertOne(game);

        const id = ref.insertedId;

        this.log.debug(`New game registered with Id: ${id}`);

        return id;
    }

    /**
     * Assigns a user to a game instance.
     */
    async assignUserToGame({
        username,
        game_code
    }) {

        const db = this.dbService.db();

        // find user instance by username
        const user = await this.usersService.getUserByUsername(username);

        if (_.isEmpty(user))
            throw new Error(`Username [${username}] is not registered`);

        // check if game code name exists
        const colGames = db.collection('games');
        let filter = { code: game_code };
        const game = await colGames.findOne(filter);

        if (_.isEmpty(game))
            throw new Error(`Game with code [${code}] is not registered`);


        // add the entity to the store
        const col = db.collection('games_users');

        filter = {
            user_id: this.dbService.toId(user._id),
            game_id: this.dbService.toId(game._id)
        };

        const update = {
            $set: {
                user_id: this.dbService.toId(user._id),
                game_id: this.dbService.toId(game._id),
                updated_at: new Date()
            }
        };

        await col.updateOne(filter, update, { upsert: true });

        this.log.debug(`User has been assigned to the game`);
    }

    /**
     * Starts a new user session with 
     * a game.
     * @param {*} param0 
     */
    async startSession(
        username,
        code
    ) {

        const db = this.dbService.db();
        const col = db.collection('games');

        // check if code name exists
        let filter = { code };
        const game = await col.findOne(filter);

        if (_.isEmpty(game))
            throw new Error(`Game with code [${code}] is not registered`);

        // find user instance by username
        const user = await this.usersService.getUserByUsername(username);

        if (_.isEmpty(user))
            throw new Error(`Username [${username}] is not registered`);

        // check if a session is already active
        const colSessions = db.collection('games_sessions');

        filter = {
            user_id: this.dbService.toId(user._id),
            game_id: this.dbService.toId(game._id),
            status: 'active'
        };

        let doc = await colSessions.findOne(filter);

        if (!_.isEmpty(doc))
            throw new Error(`An active session is already registered`);

        doc = {
            user_id: this.dbService.toId(user._id),
            game_id: this.dbService.toId(game._id),
            status: 'active',
            started_at: new Date()
        };

        // save the session entity
        const ref = await colSessions.insertOne(doc);
        const id = ref.insertedId;

        this.log.debug(`New game session created with Id: ${id}`);
    }

    /**
     * Stops a user session with 
     * a game.
     * @param {*} param0 
     */
    async stopSession(
        username,
        code
    ) {

        const db = this.dbService.db();
        const col = db.collection('games');

        // check if code name exists
        let filter = { code };
        const game = await col.findOne(filter);

        if (_.isEmpty(game))
            throw new Error(`Game with code [${code}] is not registered`);

        // find user instance by username
        const user = await this.usersService.getUserByUsername(username);

        if (_.isEmpty(user))
            throw new Error(`Username [${username}] is not registered`);

        // check if a session is already active
        const colSessions = db.collection('games_sessions');

        filter = {
            user_id: this.dbService.toId(user._id),
            game_id: this.dbService.toId(game._id),
            status: 'active'
        };

        let doc = await colSessions.findOne(filter);

        if (_.isEmpty(doc))
            throw new Error(`No active session was registered`);

        const update = {
            $set: {
                status: 'inactive',
                ended_at: new Date()
            }
        };

        // update the session entity
        await colSessions.updateOne(filter, update);

        this.log.debug(`Game session has been stopped`);
    }

    /**
  * Assigns key-value properties 
  * to a user.
  */
    async assignPropertiesToUser(
        username,
        game_code,
        properties
    ) {

        // find the user instance
        const user = await this.usersService.getUserByUsername(username);

        if (_.isEmpty(user))
            throw new Error(`User with username [${username}] is not registered`);

        const db = this.dbService.db();

        // check if game code name exists
        const colGames = db.collection('games');
        let filter = { code: game_code };
        const game = await colGames.findOne(filter);

        if (_.isEmpty(game))
            throw new Error(`Game with code [${code}] is not registered`);

        const col = db.collection('games_users');

        filter = {
            user_id: this.dbService.toId(user._id),
            game_id: this.dbService.toId(game._id)
        };

        const update = {
            $set: {
                properties,
                updated_at: new Date()
            }
        };

        await col.updateOne(filter, update);

        this.log.debug(`User properties have been set`);
    }

    /**
     * Returns the list of users 
     * in a specific game.
     * @param {*} game_code 
     */
    async getUsersInGame(
        game_code
    ) {

        const db = this.dbService.db();

        // check if game code name exists
        const colGames = db.collection('games');
        let filter = { code: game_code };
        const game = await colGames.findOne(filter);

        if (_.isEmpty(game))
            throw new Error(`Game with code [${code}] is not registered`);

        const col = db.collection('games_users');

        const pipeline = [];

        filter = {
            $match: {
                game_id: this.dbService.toId(game._id)
            }
        };

        pipeline.push(filter);

        pipeline.push({
            "$lookup": {
                "from": "users",
                "localField": "user_id",
                "foreignField": "_id",
                "as": "users"
            }
        });

        pipeline.push({
            "$project": {
                "username": { "$arrayElemAt": ["$users.username", 0] },
                "id": { "$arrayElemAt": ["$users._id", 0] },
                "_id": 0
            }
        });

        const docs = await col.aggregate(pipeline).toArray();

        return docs;
    }

    /**
       * Returns the list of users 
       * in a specific game with an 
       * active session.
       * @param {*} game_code 
       */
    async getActiveUsersInGame(
        game_code
    ) {

        const db = this.dbService.db();

        // check if game code name exists
        const colGames = db.collection('games');
        let filter = { code: game_code };
        const game = await colGames.findOne(filter);

        if (_.isEmpty(game))
            throw new Error(`Game with code [${code}] is not registered`);

        const col = db.collection('games_sessions');

        const pipeline = [];

        filter = {
            $match: {
                game_id: this.dbService.toId(game._id),
                status: 'active'
            }
        };

        pipeline.push(filter);

        pipeline.push({
            "$lookup": {
                "from": "users",
                "localField": "user_id",
                "foreignField": "_id",
                "as": "users"
            }
        });

        pipeline.push({
            "$project": {
                "username": { "$arrayElemAt": ["$users.username", 0] },
                "id": { "$arrayElemAt": ["$users._id", 0] },
                "_id": 0
            }
        });

        const docs = await col.aggregate(pipeline).toArray();

        return docs;
    }


    /**
     * Returns all or some of a 
     * user KVPs associated with 
     * a game.
     * @param {*} username 
     * @param {*} game_code 
     * @param {*} param2 
     */
    async getUserPropertiesInGame(
        username,
        game_code,
        {
            keys
        } = {}
    ) {

        // find the user instance
        const user = await this.usersService.getUserByUsername(username);

        if (_.isEmpty(user))
            throw new Error(`User with username [${username}] is not registered`);

        const db = this.dbService.db();

        // check if game code name exists
        const colGames = db.collection('games');
        let filter = { code: game_code };
        const game = await colGames.findOne(filter);

        if (_.isEmpty(game))
            throw new Error(`Game with code [${code}] is not registered`);

        const col = db.collection('games_users');

        filter = {
            user_id: this.dbService.toId(user._id),
            game_id: this.dbService.toId(game._id)
        };

        const doc = await col.findOne(filter);

        if (_.isEmpty(doc.properties))
            return [];

        let kvps = doc.properties;

        if (!_.isEmpty(keys)) {
            kvps = _.pick(kvps, keys);
        }

        return kvps;
    }



}


module.exports = GamesService;