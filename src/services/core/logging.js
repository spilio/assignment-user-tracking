const {
  createLogger,
  transports
} = require('winston');

const {
  format
} = require('logform');

/**
 * Logging service.
 *
 */
class LoggingService {

  /**
   * Constructor
   * @param {[type]} optsLogging [description]
   * @param {[type]} server      [description]
   * @param {[type]} database    [description]
   */
  constructor({
    opts
  } = {}) {

    // setup logging framework
    try {

      this.logger = createLogger({
        level: opts.logging.level,
        format: format.combine(
          format.colorize(),
          format.timestamp(),
          format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
        ),
        transports: [new transports.Console()]
      });
    } catch (e) {
      console.error(`Failed to setup logging service [${e.message}]`);
    }
  }

  /**
   * Returns the log instance.
   *
   * @return {[type]} [description]
   */
  get instance() {

    return this.logger;
  }

}

module.exports = LoggingService;
