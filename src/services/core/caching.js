const NodeCache = require('node-cache');
const _ = require('lodash');

/**
 * Caching service.
 *
 * @author Chris Spiliotopoulos
 */
class CachingService {

  /**
   * Constructor
   * @param {[type]} ttlSeconds [description]
   */
  constructor() {
    // the cache map
    this.map = {};
  }

  /**
   * Returns the cache instance.
   * @param  {[type]} cache [description]
   * @return {[type]}       [description]
   */
  _getCache(cache) {

    // if no cache name is provided,
    // return the default cache instaqnce
    if (_.isUndefined(cache)) {
      cache = 'default';
    }

    return this.map[cache];
  }

  /**
   * Returns a chached value.
   * @param  {[type]} cache [description]
   * @param  {[type]} key   [description]
   * @return {[type]}       [description]
   */
  get(cache, key) {

    const c = this._getCache(cache);

    if (_.isEmpty(c)) {
      return null;
    }

    const value = c.get(key);
    return value;
  }

  /**
   * Puts a value in a specific cache -
   * creates the cache instance if not
   * present.
   * @param {[type]} cache [description]
   * @param {[type]} key   [description]
   * @param {[type]} value [description]
   * @param {[type]} opts  [description]
   */
  set(cache, key, value, opts) {

    let c = this._getCache(cache);

    if (_.isEmpty(c)) {

      opts = opts || {};

      const ttl = (_.isUndefined(opts.ttl) ? -1 : opts.ttl);

      if (_.isEmpty(cache)) {
        cache = 'default';
      }

      // create a new cache instance
      this.map[cache] = new NodeCache({
        stdTTL: ttl,
        checkperiod: ttl * 0.2,
        useClones: false
      });
    }

    // cache the value
    c = this._getCache(cache);
    c.set(key, value);
  }

  /**
   * Deletes a cached value.
   * @param  {[type]} cache [description]
   * @param  {[type]} keys  [description]
   * @return {[type]}       [description]
   */
  del(cache, keys) {

    const c = this._getCache(cache);

    if (_.isEmpty(c)) {
      return;
    }

    c.del(keys);
  }

  /**
   * Flushes a full cache instance.
   *
   * @param  {[type]} cache [description]
   * @return {[type]}       [description]
   */
  flush(cache) {

    const c = this._getCache(cache);

    if (_.isEmpty(c)) {
      return;
    }

    c.flushAll();
  }
}


module.exports = CachingService;
