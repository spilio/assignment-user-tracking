const log = require('winston');
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const _ = require('lodash');

/**
 * Database service module.
 *
 */

class MongoService {

  /**
   * Constructor
   * @param {[type]} mongoUri The Mongo URI
   */
  constructor({
    opts,
    loggingService
  } = {}) {

    if (_.isEmpty(opts.MONGO) || _.isEmpty(opts.MONGO.URI))
        throw new Error('ENV property MONGO_URI is undefined');

    this.mongoUri = opts.MONGO.URI;
    this.logger = loggingService.instance;    
  }

  /**
   * Connects to the database.
   * @return {[type]} [description]
   */
  async connect() {

    try {
      this.logger.info(`About to connect to Mongo instance [${this.mongoUri}]`);

      // Connect to the db
      this.client = await MongoClient.connect(this.mongoUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      });

      this.logger.info('Successfully connected to mongo instance', this.mongoUri);

      // ensure all basic indexes
      this.ensureIndexes();
    } catch (e) {
      this.logger.error('Failed to connect to mongo instance', e);
    }
  }

  /**
   * Get a database connection.
   *
   * @param  {[type]} name [description]
   * @return {[type]}      [description]
   */
  db(name) {

    if (_.isEmpty(name))
      name = this.database;

    if (!this.client) {
      throw new Error('No MongoDB client instance found');
    }

    return this.client.db(name);
  }

  disconnect() {
    if (!this.db) {
      return;
    }

    this.logger.info('Disconnecting from MongoDB');

    // close the connection
    this.client.close();
  }

  /**
   * Wraps an ID value to Mongo's ObjectID
   */
   toId(value) {
    try {
      return new ObjectID(value);
    } catch (e) {}
    return null;
  }

  /**
   * Ensures all required indexes.
   *
   * @return {[type]} [description]
   */
  ensureIndexes() {

    this.logger.debug('About to ensure all indexes');

    try {


    } catch (e) {
      this.logger.error('Failed to ensure index', e.message);
    }

  }


}


module.exports = MongoService;
