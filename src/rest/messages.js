const Boom = require('@hapi/boom');
const _ = require('lodash');
const Joi = require('joi');

const EventSchema = require('../schemas/messages').Event;
const QuerySchema = require('../schemas/messages').Query;
const CommandSchema = require('../schemas/messages').Command;

/**
 * Message processing controller.
 *
 * @author Chris Spiliotopoulos
 */

class MessageController {

  constructor({
    server,
    loggingService
  } = {}) {

    this.server = server.instance;
    this.log = loggingService.instance;

    this.handlers = [];

    const { container } = server;

    // find all registered handler modules in the IoC container
    _.each(container.registrations, (entry, key) => {
      if (key.includes('Handler')) {

        // resolve the handler module within the IoC
        // and add it to the list
        const handler = container.resolve(key);
        this.handlers.push(handler);
      }
    });

    // add routing
    this.addRoutes(this);
  }

  /**
   * Adds routes.
   */
  addRoutes(self) {

    /**
    * 
    */
    this.server.route({
      path: '/messages',
      method: 'POST',
      config: {
        tags: ['api'],
        description: 'Common endpoint for processing different types of messages, i.e. Events, Commands and Queries',
        notes: 'Receives incoming messages from producers and forwards them to the appropriate message processor depending on their type.',
        validate: {
          payload: Joi.alternatives().try(EventSchema, QuerySchema, CommandSchema).required()
            .description('The message payload to be processed')
        },
        async handler(req) {

          // get the message payload
          const message = req.payload;

          try {
            // find the right handler can process this message 
            // type from the regitry and delegate
            const match = _.filter(self.handlers, h => {

              // check the message type 
              if (h.type() !== message.type)
                return false;

              // check if the handler supports this message
              if (!h.supports(message))
                return false;

              // matching handler found
              return true;
            });

            if (match.length === 0)
              throw new Error('No matching handler found for this type of message');

            // pass the message to the handler
            const [ handler ] = match;
            const response = await handler.handle(message);

            if (! response)
              return 'ok';
            else 
              return response;
              
          } catch (e) {
            self.log.error(e);
            return Boom.badRequest(e.message);
          }
        }
      }
    });



  }
}


module.exports = MessageController;
