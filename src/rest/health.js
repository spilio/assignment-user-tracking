/**
 * Health controller.
 *
 * @author Chris Spiliotopoulos
 */

class HealthController {

  constructor({
    server
  } = {}) {

    /**
     * GET /health
     *
     * Returns health status.
     */
    this.server = server.instance;

    // add routing
    this.addRoutes();
  }

  /**
   * Adds routes.
   */
  addRoutes() {

    this.server.route({
      path: '/health',
      method: 'GET',
      config: {
        auth: false,
        tags: ['api'],
        description: 'Get health status',
        notes: 'Returns status 200 OK - useful for health checks inside a cluster (e.g. kubernetes)',
        plugins: {
          'hapi-swaggered': {
            operationId: 'getHealth',
            responses: {
              200: {
                description: 'Success'
              },
              500: {
                description: 'Internal Server Error'
              }
            }
          }
        },
        handler: this.getHealth
      }
    });

  }


  /**
   * Returns the health status.
   * @return {[type]} [description]
   */
  getHealth() {
    return 'ok';
  }


}


module.exports = HealthController;
