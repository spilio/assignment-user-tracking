# Base image
FROM node:12-slim

ADD package.json /tmp/package.json

# Create the root application directory
RUN mkdir /app

COPY . /app

RUN node --version

# Install app dependencies
RUN cd app && npm install --only=production

# Set the work directory
WORKDIR /app/src

# Expose the port
EXPOSE 1330

# Start the app
CMD [ "npm", "start" ]
